console.log("Hi there! 👋");

var intro = document.getElementById("intro");
var rotateMin = -5;
var rotateMax = 5;
var rotateRange = rotateMax - rotateMin;

// fancy rotation stuff on computers with a mouse
document.addEventListener("mousemove", function(e) {
  var x = e.clientX;
  var y = e.clientY;
  x = x * rotateRange / window.innerWidth + rotateMin;
  y = y * rotateRange / window.innerHeight + rotateMin;
  intro.style.transform = "rotateX(" + y * -1 + "deg) rotateY(" + x + "deg) translateZ(-100px)";
});

// … and on touch devices with a gyro
if (window.DeviceMotionEvent) {
  window.ondeviceorientation = function(event) {
    beta = event.beta;
    gamma = event.gamma;

    setTimeout(function() {
      // (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
      y = (gamma - 90) * 40 / 180 + 20;
      x = (beta - 90) * 40 / 180 + 20;

      if (beta > rotateMin && beta < rotateMax) {
        lastTouchRotX = beta;
      }

      if (window.orientation == 0) {
        intro.style.transform = "rotateX(" + x * -1 + "deg) rotateY(" + y + "deg) translateZ(-100px)";
      } else {
        intro.style.transform = "rotateX(" + y * -1 + "deg) rotateY(" + x + "deg) translateZ(-100px)";
      }
    }, 50);
  };
}

// logo animation
var tl = anime.timeline();

tl
  // draw sprungbrett
  .add({
    targets: "#sprungbrett",
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: "easeInOutSine",
    duration: 1000
  })
  .add({
    targets: "#sprungbrett",
    fill: "#171c1a",
    easing: "easeInOutSine",
    duration: 300
  })
  // draw boy
  .add({
    targets: "#boy path",
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: "easeInOutSine",
    duration: 1300,
    offset: "-=500"
  })
  .add({
    targets: "#boy path",
    fill: "#171c1a",
    easing: "easeInOutSine",
    duration: 300
  })
  // draw bg circle
  .add({
    targets: "#kreis path",
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: "easeInOutSine",
    duration: 1300,
    offset: "-=200"
  })
  .add({
    targets: "#kreis path",
    fill: "#171c1a",
    easing: "easeInOutSine",
    duration: 300
  });
